/**
 * Badge per hacklabcormano
 *
 * BOARD = lolin esp32 (con OLED)
 */
 
 
/* per esp8266+tm1638
 * usare TM1638lite tm(D2, D1, D0); FUNZIONA esempio
 * cfr. anche https://github.com/pasz1972/HassMqqtAlarmPanelTM1638
 * 
 */

//#define DEBUG

// per configurare tipo di display (e input buttons)
#define TM
//#define OLED

////////////////////////////////////////////////////////////////
long maxHeap=0;

////////////////////////////////////////////////////////////////
#include "logo.h"

////////////////////////////////////////////////////////////////
const String Root = "Hacklabcormano.it/Badge";
const String User = "atrent";
const String Presenza = "/Presente";
// TODO altre stringhe

////////////////////////////////////////////////////////////////
//const char MQTT[] ="mqtt.hacklabcormano.it";
const char MQTT[] ="172.16.1.1";
//const char MQTT[] ="routercasa";
String lastPayload;

////////////////////////////////////////////////////////////////
const char SEPA[]="/";
String presenze;
boolean presente(String user) {
    return presenze.indexOf(user)!=-1;
}

void arriva(String user) {
    //Serial.println("arriva...");
    if(!presente(user)) {
        presenze.concat(user);
        presenze.concat(SEPA);
        heapPercent();

        Serial.print("arrivato: ");
        Serial.println(user);
        Serial.print("presenze: ");
        Serial.println(presenti());
    }
}

int presenti() {
    if(presenze.length()==0)
        return -1;

#ifdef DEBUG
    Serial.print("indexof: ");
    Serial.println(presenze.indexOf(SEPA));
#endif

    int pres=0;
    for(
        int from=0;
        (from=presenze.indexOf(SEPA,from)+1)!=0;
        pres++
    ) {
#ifdef DEBUG
        Serial.print(from);
        Serial.print(",");
        Serial.println(pres);
        delay(100);
#endif
    }
    return pres;
}

////////////////////////////////////////////////////////////////
#include "wifi.h"
// oppure definire:
//const char SSID[] ="...";
//const char PWD[] ="...";


/* per wifi provare (ma serve davvero?):
 * https://github.com/Hieromon/AutoConnect
 * NO https://github.com/nrwiersma/ConfigManager
 * NO https://github.com/madhephaestus/Esp32WifiManager
 * NO! (nodered?!?) https://github.com/dreed47/WifiMQTTManager
 * https://github.com/prampec/IotWebConf
 */

////////////////////////////////////////////////////////////////
// Include the correct display library
// For a connection via I2C using Wire include
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
// or #include "SH1106.h" alis for `#include "SH1106Wire.h"`
// For a connection via I2C using brzo_i2c (must be installed) include
// #include <brzo_i2c.h> // Only needed for Arduino 1.6.5 and earlier
// #include "SSD1306Brzo.h"
// #include "SH1106Brzo.h"
// For a connection via SPI include
// #include <SPI.h> // Only needed for Arduino 1.6.5 and earlier
// #include "SSD1306Spi.h"
// #include "SH1106SPi.h"

// Initialize the OLED display using SPI
// D5 -> CLK
// D7 -> MOSI (DOUT)
// D0 -> RES
// D2 -> DC
// D8 -> CS
// SSD1306Spi        display(D0, D2, D8);
// or
// SH1106Spi         display(D0, D2);

// Initialize the OLED display using brzo_i2c
// D3 -> SDA
// D5 -> SCL
// SSD1306Brzo display(0x3c, D3, D5);
// or
// SH1106Brzo  display(0x3c, D3, D5);

// Initialize the OLED display using Wire library
#ifdef ESP32
SSD1306  display(0x3c, 5, 4);
#else
SSD1306  display(0x3c, D2, D1);
#endif
//SH1106 display(0x3c, D3, D5);

const int width=display.getWidth();
const int height=display.getHeight();

////////////////////////////////////////////////////////////////
#include <TaskScheduler.h>
Scheduler runner;

#ifdef OLED
/** ora solo per demo/test, poi visualizzerà menu */
Task OledTask(100*TASK_MILLISECOND, TASK_FOREVER, oledUpdate);
#endif

// animazione per far vedere che è attivo
short cursor=0;
short direction=1;
Task CylonTask(10*TASK_MILLISECOND, TASK_FOREVER, []() {
    cursor+=direction;

    if(cursor==width) direction=-1;
    if(cursor==0) direction=1;

    /* per ora fatto con progress bar
    progress--;
    if(progress<0) {
        progress=100;
        display.clear();
    }
    display.drawProgressBar(0,height-5, width-1, 4, progress);
    */
});

////////////////////////////////////////////////////////////////
/* mqtt:
 * https://github.com/merlinschumacher/Basecamp (NO)
 * https://github.com/plapointe6/EspMQTTClient (questo! ma NON supporta wildcard, nel senso che fa subscribe ma poi non chiama la callback)
 * oltre al solito pubsub
 */

#include "EspMQTTClient.h"
EspMQTTClient mqttclient(
    SSID,
    PWD,
    MQTT,  // MQTT Broker server ip
    User.c_str(),     // Client name that uniquely identify your device
    1883              // The MQTT port, default to 1883. this line can be omitted
);
Task MqttKeepaliveTask(100*TASK_MILLISECOND, TASK_FOREVER, []() {
    mqttclient.loop();
});
Task MqttSendTask(10*TASK_SECOND, TASK_FOREVER, []() {
    mqttclient.publish("atrent/millis", String(millis())); // tanto per vedere qualcosa periodicamente
});
Task PresenzaTask(10*TASK_SECOND, TASK_FOREVER, []() {
#ifdef PRESENZAFAKE
    mqttclient.publish(Root+Presenza,User+millis()); // test (per far vedere tante presenze)
#else
    mqttclient.publish(Root+Presenza,User); // definitivo
#endif
    heapPercent();

#ifdef DEBUG
    Serial.print("stringa presenti: ");
    Serial.println(presenze);
    heapPercent();
#endif
});

////////////////////////////////////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println("< booting...");


    // Optional functionnalities of EspMQTTClient :
    mqttclient.enableDebuggingMessages(); // Enable debugging messages sent to serial output
    //mqttclient.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overrited with enableHTTPWebUpdater("user", "password").
    mqttclient.enableLastWillMessage("TestClient/lastwill", "I am going offline");  // You can activate the retain flag by setting the third parameter to true

    // tasks
    runner.init();

    runner.addTask(MqttKeepaliveTask);
    MqttKeepaliveTask.enable();

    runner.addTask(MqttSendTask);
    MqttSendTask.enable();

#ifdef OLED
    oledSetup();
    runner.addTask(OledTask);
    OledTask.enable();
    runner.addTask(CylonTask);
    CylonTask.enable();
#endif

    runner.addTask(PresenzaTask);
    PresenzaTask.enable();

    Serial.println("setup complete! >");
}

////////////////////////////////////////////////////////////////
void loop() {
    runner.execute(); // TaskScheduler
}



////////////////////////////////////////////////////////////////
// This function is called once everything is connected (Wifi and MQTT)
// WARNING : YOU MUST IMPLEMENT IT IF YOU USE EspMQTTClient
void onConnectionEstablished() {
    /* Subscribe to "mytopic/test" and display received message to Serial
    mqttclient.subscribe("mytopic/test", [](const String & payload) {
        Serial.println(payload);
    });
    */

    // wildcard
    mqttclient.subscribe("atrent/#", [](const String & payload) {
        Serial.print(">>> mqtt wildcard: ");
        Serial.println(payload);
        //lastPayload=payload;
    });

    // get presenze
    mqttclient.subscribe(Root+Presenza, [](const String & payload) {
        Serial.print("+++ mqtt presenza: ");
        Serial.println(payload);
        arriva(payload);
        lastPayload=payload;
    });

    /* prove
    mqttclient.subscribe("msg", [](const String & payload) {
        Serial.print("msg: ");
        Serial.println(payload);
    });
    */

    /* Publish a message to "mytopic/test"
    mqttclient.publish("mytopic/test", "This is a message"); // You can activate the retain flag by setting the third parameter to true
    */

    /* Execute delayed instructions
    mqttclient.executeDelayed(5 * 1000, []() {
        mqttclient.publish("mytopic/test", "This is a message sent 5 seconds later");
    });
    */
}

#ifdef OLED
void oledUpdate() {
    // TODO rendere + parametriche posizioni

    display.clear();

    //display.drawXbm((width-HLCpittNero_width)/2, 0, HLCpittNero_width, HLCpittNero_height, HLCpittNero_bits);
    display.drawXbm(0, 0, HLCpittNero_width, HLCpittNero_height, HLCpittNero_bits);

    display.drawString(HLCpittNero_width+5, 0, User);

    display.drawString(HLCpittNero_width+5, 20, "p: "+String(presenti()));

    display.drawString(0, HLCpittNero_height+3, "last: "+lastPayload);

    display.drawProgressBar(0,height-10, width-1, 4, heapPercent());

    //display.setPixel(cursor,height-1);
    //display.setColor(WHITE);
    display.fillCircle(cursor,height-3, 2);

    display.display(); // serve!
}

void oledSetup() {
    // Initialising the UI will init the display too.
    display.init();

    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_16);

    display.clear();
}
#endif

/*
void plotterCallback() {
    // TODO ragionare sulle scale di rappresentazione

    Serial.print(rpm());

    Serial.print(",");
    Serial.print(desiredRpm);

    Serial.print(",");
    Serial.print(potenziometro);

    Serial.print(",");
    Serial.print(absDuty);

    Serial.print(",");
    Serial.print(pidResult);

    Serial.print(",");
    Serial.print(period/1000);

    Serial.println();
}

void statusCallback() {
    Serial.println("---------------------------");

    Serial.print("rpm: ");
    Serial.println(rpm());

    Serial.print("desiredRpm: ");
    Serial.println(desiredRpm);

    Serial.print("absDuty: ");
    Serial.println(absDuty);

    Serial.print("lastPulse: ");
    Serial.println(lastPulse);

    Serial.print("lastTick: ");
    Serial.println(lastTick);

    Serial.print("period: ");
    Serial.println(period);

    // cursor e photovalues non ha senso per ora
}
*/

short heapPercent() {
    long h=ESP.getFreeHeap();
    if(h>maxHeap) maxHeap=h;
    short hp=map(h,0,maxHeap,0,100);

#ifdef DEBUGHEAP
    Serial.print("Heap free: ");
    Serial.print(h);
    Serial.print(" of max ");
    Serial.print(maxHeap);
    Serial.print(" (");
    Serial.print(hp);
    Serial.println("%)");
#endif

    return hp;
}
