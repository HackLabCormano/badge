// convert HLCpittNero.svg -resize 80x50 HLCpittNero.ico

#define HLCpittNero_width 55
#define HLCpittNero_height 34
const uint8_t HLCpittNero_bits[] = {
    0x00, 0xF0, 0x1F, 0x00, 0xF8, 0x07, 0x00, 0x00, 0xFE, 0x7F, 0x00, 0xFF,
    0x3F, 0x00, 0x80, 0xFF, 0xFF, 0xC1, 0xFF, 0xFF, 0x00, 0xC0, 0xFF, 0xFF,
    0xF7, 0xFF, 0xFF, 0x01, 0xE0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0xF8,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0xF8, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0x0F, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1F, 0xFE, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0x3F, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1F,
    0xF0, 0x1F, 0xF8, 0xFF, 0x0F, 0xFC, 0x03, 0x80, 0x0F, 0xF0, 0xFF, 0x47,
    0xF8, 0x00, 0x00, 0x0C, 0xE0, 0xFF, 0xC3, 0x18, 0x00, 0x00, 0x80, 0xE3,
    0xFF, 0xC3, 0x00, 0x00, 0x00, 0xC0, 0xE3, 0xFF, 0xC3, 0x00, 0x00, 0x00,
    0xC0, 0xC3, 0xFF, 0xC1, 0x00, 0x00, 0x00, 0xC0, 0xE3, 0xFF, 0xC3, 0x00,
    0x00, 0x00, 0x04, 0xE0, 0xFF, 0xC3, 0x30, 0x00, 0x80, 0x0F, 0xF0, 0xFF,
    0x47, 0x78, 0x00, 0xE0, 0x1F, 0xF8, 0xFF, 0x0F, 0xFC, 0x03, 0xF8, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F,
    0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x3F, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1F, 0xF8, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0x0F, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0xF0,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0xE0, 0xFF, 0xFF, 0xF3, 0xFF, 0xFF,
    0x01, 0x00, 0xFF, 0xFF, 0xC1, 0xFF, 0xFF, 0x00, 0x00, 0xFE, 0x7F, 0x00,
    0xFF, 0x3F, 0x00, 0x00, 0xF0, 0x0F, 0x00, 0xF8, 0x07, 0x00,
};

/*
#define HLCpittNero_width 80
#define HLCpittNero_height 50
const uint8_t
//static char
HLCpittNero_bits[] = {
    0x00, 0x00, 0xFC, 0x1F, 0x00, 0x00, 0xF8, 0x1F, 0x00, 0x00, 0x00, 0x80,
    0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00, 0xE0, 0xFF, 0xFF,
    0x03, 0xC0, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0xF8, 0xFF, 0xFF, 0x0F, 0xF0,
    0xFF, 0xFF, 0x1F, 0x00, 0x00, 0xFE, 0xFF, 0xFF, 0x3F, 0xFC, 0xFF, 0xFF,
    0x7F, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x7F, 0xFE, 0xFF, 0xFF, 0xFF, 0x00,
    0xC0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0xC0, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0xE0, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0x0F, 0xF8, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x1F, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F,
    0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFE, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFE, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x7F, 0xF0, 0xFF, 0x5F, 0xFE, 0xFF, 0xFF, 0x3F, 0xFC, 0xFF, 0x0F,
    0x80, 0xFF, 0x07, 0xF0, 0xFF, 0xFF, 0x1F, 0xE0, 0xFF, 0x01, 0x00, 0xFE,
    0x03, 0xE0, 0xFF, 0xFF, 0x07, 0xC1, 0x7F, 0x00, 0x00, 0xF0, 0x01, 0xC0,
    0xFF, 0xFF, 0x83, 0x83, 0x0F, 0x00, 0x00, 0x80, 0x01, 0xC0, 0xFF, 0xFF,
    0x83, 0x83, 0x01, 0x00, 0x00, 0x00, 0xE0, 0x83, 0xFF, 0xFF, 0x81, 0x03,
    0x00, 0x00, 0x00, 0x00, 0xE0, 0x03, 0xFF, 0xFF, 0x81, 0x03, 0x00, 0x00,
    0x00, 0x00, 0xF0, 0x87, 0xFF, 0xFF, 0x81, 0x03, 0x00, 0x00, 0x00, 0x00,
    0xF0, 0x87, 0xFF, 0xFF, 0x80, 0x03, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x83,
    0xFF, 0xFF, 0x81, 0x03, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x83, 0xFF, 0xFF,
    0x81, 0x03, 0x00, 0x00, 0x00, 0x80, 0x00, 0xC0, 0xFF, 0xFF, 0x83, 0x03,
    0x01, 0x00, 0x00, 0xF0, 0x01, 0xC0, 0xFF, 0xFF, 0x83, 0x83, 0x0F, 0x00,
    0x00, 0xFC, 0x03, 0xE0, 0xFF, 0xFF, 0x07, 0xC2, 0x3F, 0x00, 0x80, 0xFF,
    0x07, 0xF0, 0xFF, 0xFF, 0x1F, 0xE0, 0xFF, 0x01, 0xE0, 0xFF, 0x3F, 0xFD,
    0xFF, 0xFF, 0x3F, 0xFD, 0xFF, 0x07, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F,
    0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFC, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFC, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xF8, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0x1F, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x0F, 0xE0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0F,
    0xE0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0x80, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0x00, 0xFF, 0xFF, 0xFF,
    0x7F, 0xFE, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFE, 0xFF, 0xFF, 0x3F, 0xFC,
    0xFF, 0xFF, 0x7F, 0x00, 0x00, 0xFC, 0xFF, 0xFF, 0x0F, 0xF0, 0xFF, 0xFF,
    0x1F, 0x00, 0x00, 0xE0, 0xFF, 0xFF, 0x03, 0xC0, 0xFF, 0xFF, 0x07, 0x00,
    0x00, 0x80, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00,
    0xFC, 0x17, 0x00, 0x00, 0xF0, 0x1F, 0x00, 0x00,
};
*/
