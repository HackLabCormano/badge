# Badge (ESP8266 o ESP32 + OLED + qualche pulsante)

Un piccolo "badge" per i soci, che si connetta ad un server (mqtt?) per segnalare la propria presenza alle serate e per interagire ad esempio durante un seminario ("alzo la mano", etc.)

Se connesso alla rete wifi dell'hacklab e usando il gw come mqtt broker resta tutto interno.



## Use case

* contare presenze
* segnalare richiesta di aiuto (?)
* alzare la mano per fare una domanda durante seminario (ganzo, così si può realizzare un sw di gestione incontri)
* votare una mozione (idem, sw di raccolta voti, non anonimo naturalmente, voto palese)

Ovviamente serve un server che raccolga queste info e le presenti, specie il tener traccia delle alzate di mano.



## Architettura

Nota bene: in realtà il "badge" può essere un qualunque device (anche un PC o un telefono) che sappia parlare MQTT e che rispetti il protocollo (cfr. sezione "Topic") di interazione.

Badge: ESPxxx + OLED +  qualche pulsante (interfaccia utente, menu, azioni)

Server minimale: mqtt broker

Server "logico": mqtt client che legge i messaggi e "calcola proprietà"

Il badge potrebbe essere anche un subscriber per qualche topic per ricevere notifiche o anche per calcolare in proprio qualche proprietà (ad esempio il numero di presenze) e visualizzarla sull'OLED.

Alternativa HW https://hackaday.com/2019/05/12/open-hardware-e-ink-display-just-needs-an-idea/



### UI

Si può usare un solo pulsante e usare la durata della pressione per distinguere semantica, ad es.:

* momentary press: cicla tra voci/opzioni (approvazione, disapprovazione, etc.)
* long press: invia scelta corrente

In alternativa un "rotary encoder" o un "joystick" (tipo di quelli presenti in molti kit).

In alternativa si può realizzare interazione via Bluetooth con un'app del tel (che però rende più complesso il tutto e aggiunge onere di sviluppo).



## Topic (ipotesi stringhe), protocollo di interazione con MQTT server

* Root: Hacklabcormano.it/Badge/
* User: <nickname> (impostato dal socio)
* Presenza: <Root>/Presente <User> (inviato periodicamente)
* Approvazione: <Root>/Approvo <User> (inviato solo su pulsante)
* Non approvazione: <Root>/NonApprovo <User> (inviato solo su pulsante)
* Alzata di mano: <Root>/ManoAlzata <User> (inviato solo su pulsante)

I singoli badge possono anche "ascoltare" (subscribe) i vari topic per effettuare computazioni locali (ad es. numero dei presenti).



## Librerie

da verificare se sono OK

* wifi e mqtt, https://github.com/plapointe6/EspMQTTClient (non gestisce wildcard)
* oled (basso livello), https://github.com/ThingPulse/esp8266-oled-ssd1306
* oled menu, https://github.com/neu-rah/ArduinoMenu (?) (forse esagerato)



## TODO

* adattarsi a OLED di varia dimensione (per usare anche wemos+oled ad esempio)
* sperimentare e-ink?
* pulsanti
* case "figo"
